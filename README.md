### Looking for group match maker.


### Backend
- User looking for playing
   - Form to create game
      - Would need discord link
      - Name of the game (title)
      - Description
      - Per player limit*
      - Estimated length time*
- Store Users in DB
- Store Games created by users
- Match Users to Games
   - Join Button
   - Leave Button
- Rate limit in creating games
- If a user leaves a game the user should not be matched to that game when wanting to join again.
- Users would be able to friend other users

### UI
   - One UI with 2 buttons
      - User creating the game
      - User joining the game
   - *Log showing all games played*
   - Reset Password
   - Party System
      - Wait for everyone to connect
      - Join Game together
   - Give creators ability to mark spots as taken
   - Allow Joiners to join as more than one slot
   - Users can see a listing of all added friends
      - Will be able to see who's online ( use of web sockets )
      - Will be able to join current game if slots available